/*jshint node:true*/
'use strict';

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'pt-login',
    environment: environment
  }

  return ENV;
};
